Meteor.startup(function(){
  if(States.find().count() === 0) {
    JSON.parse(Assets.getText("states.json")).state.forEach(function (doc) {
      States.insert(doc);
    });
  }
    if(Votes.find().count() === 0) {
      JSON.parse(Assets.getText("votes.json")).state.forEach(function (doc) {
        Votes.insert(doc);
      });
    }
});
