Meteor.methods({
  toggleState:function(state){
    var res = States.findOne({code:state}).result;
    if(res == "none")
      res = "gop";
    else if(res == "gop")
      res = "dem";
    else if(res == "dem")
      res = "none";

    States.update({code:state}, {$set:{
      result: res
    }});
  },
  getSim: function(simType) {
    return JSON.parse(Assets.getText("states.json"));
  }
});
