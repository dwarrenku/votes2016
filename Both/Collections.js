Votes = new Mongo.Collection("votes");
States = new Mongo.Collection("states");
Meteor.users.deny({
  insert: function(){
    return true;
  },
  update: function(){
    return true;
  },
  remove: function(){
    return true;
  }
});
