FlowRouter.route('/', {
    action: function() {
      BlazeLayout.render('mainLayout',{main: 'map'});
    },
    name: 'map'
});

FlowRouter.route('/simulation', {
    action: function() {
      BlazeLayout.render('mainLayout',{main: 'simulation'});
    },
    name: 'simulation'
});
