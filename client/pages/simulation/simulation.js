Template.simulation.onCreated(
  function() {
    Simulation = new Mongo.Collection(null);
        election2008.state.forEach(function (doc) {
          Simulation.insert(doc);
        });
  });

Template.simulation.events({
  'click .state' : function(event, template) {
    var res = this.result;
    if(res == "none")
      res = "gop";
    else if(res == "gop")
      res = "dem";
    else if(res == "dem")
      res = "none";

    Simulation.update({code:this.code}, {$set:{
      result: res
    }});
  },
  'mouseover .state' : function(event, template) {
    //console.log(event.target);
      $(event.target).popover({
        title: event.target.id,
        content: this.votes + " electoral votes",
        container: "body",
        trigger: "hover",
        placement: "auto bottom"
      });
      $(event.target).popover("show");
  }
});

Template.simulation.helpers({
  states: function(){
    return Simulation.find();
  },
  progress: function(party){
    var votes;
    if(party == "dem") {
      votes = 0;
      Simulation.find({result: "dem"}).forEach(function(state){
        votes += parseInt(state.votes);
      });
      Simulation.find({result: "mixed"}).forEach(function(state){
        votes += parseInt(state.votes.dem);
      });
      return (votes/538)*100;
    } else if (party == "gop") {
      votes = 0;
      Simulation.find({result: "gop"}).forEach(function(state){
        votes += parseInt(state.votes);
      });
      Simulation.find({result: "mixed"}).forEach(function(state){
        votes += parseInt(state.votes.gop);
      });
      return (votes/538)*100;
    } else if (party == "none") {
      votes = 0;
      Simulation.find({result: "none"}).forEach(function(state){
        votes += parseInt(state.votes);
      });
      return (votes/538)*100;
    }
  },
  votes: function(party){
    var votes;
    if(party == "dem") {
      votes = 0;
      Simulation.find({result: "dem"}).forEach(function(state){
        votes += parseInt(state.votes);
      });
      Simulation.find({result: "mixed"}).forEach(function(state){
        votes += parseInt(state.votes.dem);
      });
      return votes;
    } else if(party == "gop") {
      votes = 0;
      Simulation.find({result: "gop"}).forEach(function(state){
        votes += parseInt(state.votes);
      });
      Simulation.find({result: "mixed"}).forEach(function(state){
        votes += parseInt(state.votes.gop);
      });
      return votes;
    } else if(party == "none") {
      votes = 0;
      Simulation.find({result: "none"}).forEach(function(state){
        votes += parseInt(state.votes);
      })
      return votes;
    }
  },
  fill: function(){
    var result = Simulation.findOne({code: this.code}).result;
    if(result == "dem"){
      return "#20658D";
    } else if(result == "gop") {
      return "#E02F2F";
    } else if(result == "none") {
      return "#D3D3D3";
    } else if(result == "mixed") {
      return "#9A2B5D"
    }
  }
});
