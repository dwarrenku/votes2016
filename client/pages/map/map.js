Template.map.events({
  'click .state' : function(event, template) {
    Meteor.call("toggleState", this.code, function(error, result){
      if(error){
        console.log("error", error);
      }
      if(result){

      }
    });
  },
  'mouseover .state' : function(event, template) {
    //console.log(event.target);
      $(event.target).popover({
        title: event.target.id,
        content: this.votes + " electoral votes",
        container: "body",
        trigger: "hover",
        placement: "auto bottom"
      });
      $(event.target).popover("show");
  }
});

Template.map.helpers({
  states: function(){
    return States.find();
  },
  progress: function(party){
    var votes;
    if(party == "dem") {
      votes = 0;
      States.find({result: "dem"}).forEach(function(state){
        votes += parseInt(state.votes);
      })
      return (votes/538)*100;
    } else if (party == "gop") {
      votes = 0;
      States.find({result: "gop"}).forEach(function(state){
        votes += parseInt(state.votes);
      })
      return (votes/538)*100;
    } else if (party == "none") {
      votes = 0;
      States.find({result: "none"}).forEach(function(state){
        votes += parseInt(state.votes);
      })
      return (votes/538)*100;
    }
  },
  votes: function(party){
    var votes;
    if(party == "dem") {
      votes = 0;
      States.find({result: "dem"}).forEach(function(state){
        votes += parseInt(state.votes);
      })
      return votes;
    } else if(party == "gop") {
      votes = 0;
      States.find({result: "gop"}).forEach(function(state){
        votes += parseInt(state.votes);
      })
      return votes;
    } else if(party == "none") {
      votes = 0;
      States.find({result: "none"}).forEach(function(state){
        votes += parseInt(state.votes);
      })
      return votes;
    }
  },
  fill: function(){
    var result = States.findOne({code: this.code}).result;
    if(result == "dem"){
      return "#20658D";
    } else if(result == "gop") {
      return "#E02F2F";
    } else if(result == "none") {
      return "#D3D3D3";
    }
  }
});
