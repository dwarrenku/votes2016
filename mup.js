module.exports = {
  servers: {
    one: {
      host: '45.55.132.76',
      username: 'root',
      pem: 'C:/Users/dwarren/.ssh/id_rsa'
      // or leave blank for authenticate from ssh-agent
    }
  },

  meteor: {
    name: 'votes',
    path: '.',
    dockerImage: 'abernix/meteord:base',
    servers: {
      one: {}
    },
    buildOptions: {
      serverOnly: true,
    },
    env: {
      ROOT_URL: 'http://45.55.132.76',
      MONGO_URL: 'mongodb://localhost/meteor',
      PORT: "8080"
    },

    //dockerImage: 'kadirahq/meteord'
    deployCheckWaitTime: 60
  },

  mongo: {
    oplog: true,
    port: 27017,
    servers: {
      one: {},
    },
  },
};
